package betterwithmods.hcbuoy;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import net.minecraft.block.Block;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.item.EnumDyeColor;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.Ingredient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.common.config.Property;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInterModComms;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.OreIngredient;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Mod(modid = "hardcorebuoy", name = "Hardcore Buoy", version = "${version}", acceptedMinecraftVersions = "[1.12, 1.13)")
public class HCBuoy {

    public static StackMap<Float> buoyancy;
    public static Configuration config;
    private static List<String> _defaults = Lists.newArrayList();

    public static float getBuoyancy(ItemStack stack) {
        return buoyancy.get(stack);
    }

    private static Ingredient ingredientfromString(String name) {
        if (name.startsWith("ore:"))
            return new OreIngredient(name.substring(4));
        String[] split = name.split(":");
        if (split.length > 1) {
            int meta = 0;
            if (split.length > 2) {
                meta = Integer.parseInt(split[2]);
            }
            Item item = ForgeRegistries.ITEMS.getValue(new ResourceLocation(split[0], split[1]));
            if (item != null) {
                return Ingredient.fromStacks(new ItemStack(item, 1, meta));
            }
        }
        return Ingredient.EMPTY;
    }

    public static String[] loadPropStringList(String propName, String category, String desc, String[] default_) {
        Property prop = config.get(category, propName, default_);
        prop.setComment(desc);

        return prop.getStringList();
    }

    public static HashMap<Ingredient, Float> loadItemStackIntMap(String propName, String category, String desc, String[] _default) {
        HashMap<Ingredient, Float> map = Maps.newHashMap();
        for (String s : loadPropStringList(propName, category, desc, _default)) {
            String[] a = s.split("=");
            if (a.length == 2) {
                map.put(ingredientfromString(a[0]), Float.parseFloat(a[1]));
            }
        }
        return map;
    }

    private static String fromStack(ItemStack stack) {
        if (stack.getMetadata() == 0) {
            return stack.getItem().getRegistryName().toString();
        } else {
            return String.format("%s:%s", stack.getItem().getRegistryName(), stack.getMetadata());
        }
    }

    private static void addDefaultOre(String ore, float buoy) {
        _defaults.add(String.format("ore:%s=%s", ore, buoy));
    }

    private static void addDefaultStack(ItemStack stack, float buoy) {

        _defaults.add(String.format("%s=%s", fromStack(stack), buoy));
    }

    private static void addDefaultBlock(Block block, float buoy) {
        addDefaultStack(new ItemStack(block), buoy);
    }

    private static void addDefaultBlock(Block block, int meta, float buoy) {
        addDefaultStack(new ItemStack(block, 1, meta), buoy);
    }

    private static void addDefaultItem(Item item, float buoy) {
        addDefaultStack(new ItemStack(item), buoy);
    }

    private static void addDefaultItem(Item item, int meta, float buoy) {
        addDefaultStack(new ItemStack(item, 1, meta), buoy);
    }

    @Mod.EventHandler
    public void intermod(FMLInterModComms.IMCEvent event) {
        for (FMLInterModComms.IMCMessage message : event.getMessages()) {
            if (message.key.equals("buoy")) {
                NBTTagCompound tag = message.getNBTValue();
                float value = tag.getFloat("value");

                if (tag.hasKey("stack")) {
                    ItemStack stack = new ItemStack(tag.getCompoundTag("stack"));
                    buoyancy.put(stack, value);
                } else if (tag.hasKey("oredict")) {
                    String ore = tag.getString("oredict");
                    buoyancy.put(ore, value);
                }
            }
        }
    }

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        config = new Configuration(event.getSuggestedConfigurationFile());
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        config.load();
        float defaultBuoyancy = config.getFloat("Default Buoyancy", Configuration.CATEGORY_GENERAL, -1.0f, -1.0f, 1.0f, "The default buoyancy that all items with have if not explicitly added to the config.");
        buoyancy = new StackMap<>(defaultBuoyancy);

        //Oredict
        addDefaultOre("logWood", 1.0F);
        addDefaultOre("leather", 1.0F);
        addDefaultOre("slimeball", 0.0F);
        addDefaultOre("bone", 1.0F);
        addDefaultOre("gearWood", 1.0F);
        addDefaultOre("foodFlour", 1.0F);
        addDefaultOre("string", 1.0F);
        addDefaultOre("fabricHemp", 1.0F);
        addDefaultOre("barkWood", 1.0F);
        addDefaultOre("blockCactus", 1.0F);
        addDefaultOre("sugarcane", 1.0F);
        addDefaultOre("vine", 1.0F);
        addDefaultOre("stick", 1.0F);
        addDefaultOre("feather", 1.0F);
        addDefaultOre("plankWood", 1.0F);
        addDefaultOre("treeSapling", 1.0F);
        addDefaultOre("treeLeaves", 1.0F);
        addDefaultOre("torch", 1.0F);
        addDefaultOre("stairWood", 1.0F);
        addDefaultOre("chest", 1.0F);
        addDefaultOre("workbench", 1.0F);
        addDefaultOre("cropWheat", 1.0F);
        addDefaultOre("cropPotato", 1.0F);
        addDefaultOre("cropCarrot", 1.0F);
        addDefaultOre("cropNetherWart", 1.0F);


        //Items
        addDefaultItem(Items.APPLE, 1.0F);
        addDefaultItem(Items.BOW, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.ARROW, 1.0F);
        addDefaultItem(Items.WOODEN_SWORD, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.WOODEN_SHOVEL, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.WOODEN_PICKAXE, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.WOODEN_AXE, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.BOWL, 1.0F);
        addDefaultItem(Items.MUSHROOM_STEW, 1.0F);
        addDefaultItem(Items.BEETROOT_SOUP, 1.0F);
        addDefaultItem(Items.WOODEN_HOE, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.BEETROOT_SEEDS, 1.0F);
        addDefaultItem(Items.MELON_SEEDS, 1.0F);
        addDefaultItem(Items.PUMPKIN_SEEDS, 1.0F);
        addDefaultItem(Items.WHEAT_SEEDS, 1.0F);
        addDefaultItem(Items.BREAD, 1.0F);
        addDefaultItem(Items.LEATHER_HELMET, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.LEATHER_CHESTPLATE, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.LEATHER_LEGGINGS, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.LEATHER_BOOTS, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.PORKCHOP, 1.0F);
        addDefaultItem(Items.COOKED_PORKCHOP, 1.0F);
        addDefaultItem(Items.PAINTING, 1.0F);
        addDefaultItem(Items.SIGN, 1.0F);
        addDefaultItem(Items.SADDLE, 1.0F);
        addDefaultItem(Items.SNOWBALL, 1.0F);
        addDefaultItem(Items.BOAT, 1.0F);
        addDefaultItem(Items.ACACIA_BOAT, 1.0F);
        addDefaultItem(Items.BIRCH_BOAT, 1.0F);
        addDefaultItem(Items.DARK_OAK_BOAT, 1.0F);
        addDefaultItem(Items.JUNGLE_BOAT, 1.0F);
        addDefaultItem(Items.SPRUCE_BOAT, 1.0F);
        addDefaultItem(Items.PAPER, 1.0F);
        addDefaultItem(Items.BOOK, 1.0F);
        addDefaultItem(Items.FISHING_ROD, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.FISH, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.COOKED_FISH, OreDictionary.WILDCARD_VALUE, 1.0F);
        addDefaultItem(Items.SUGAR, 1.0F);
        addDefaultItem(Items.COOKIE, 1.0F);
        addDefaultItem(Items.MAP, 1.0F);
        addDefaultItem(Items.MELON, 1.0F);
        addDefaultItem(Items.BEEF, 1.0F);
        addDefaultItem(Items.COOKED_BEEF, 1.0F);
        addDefaultItem(Items.CHICKEN, 1.0F);
        addDefaultItem(Items.COOKED_CHICKEN, 1.0F);
        addDefaultItem(Items.MUTTON, 1.0F);
        addDefaultItem(Items.COOKED_MUTTON, 1.0F);
        addDefaultItem(Items.ROTTEN_FLESH, 1.0F);
        addDefaultItem(Items.POTIONITEM, 1.0F);
        addDefaultItem(Items.GLASS_BOTTLE, 1.0F);
        addDefaultItem(Items.SPIDER_EYE, 1.0F);
        addDefaultItem(Items.FERMENTED_SPIDER_EYE, 1.0F);
        addDefaultItem(Items.MAGMA_CREAM, 1.0F);
        addDefaultItem(Items.WRITABLE_BOOK, 1.0F);
        addDefaultItem(Items.WRITTEN_BOOK, 1.0F);
        addDefaultItem(Items.ITEM_FRAME, 1.0F);
        addDefaultItem(Items.FLOWER_POT, 1.0F);
        addDefaultItem(Items.BAKED_POTATO, 1.0F);
        addDefaultItem(Items.POISONOUS_POTATO, 1.0F);
        addDefaultItem(Items.FILLED_MAP, 1.0F);
        addDefaultItem(Items.SKULL, 1.0F);
        addDefaultItem(Items.CARROT_ON_A_STICK, 1.0F);
        addDefaultItem(Items.PUMPKIN_PIE, 1.0F);
        addDefaultItem(Items.DYE, 1.0F);
        addDefaultItem(Items.SIGN, 1.0F);
        addDefaultItem(Items.BED, 1.0F);
        addDefaultItem(Items.ACACIA_DOOR, 1.0F);
        addDefaultItem(Items.BIRCH_DOOR, 1.0F);
        addDefaultItem(Items.DARK_OAK_DOOR, 1.0F);
        addDefaultItem(Items.JUNGLE_DOOR, 1.0F);
        addDefaultItem(Items.OAK_DOOR, 1.0F);
        addDefaultItem(Items.SPRUCE_DOOR, 1.0F);
        addDefaultItem(Items.CAKE, 1.0F);
        addDefaultItem(Items.CARROT, 1.0F);
        addDefaultItem(Items.DYE, EnumDyeColor.BROWN.getDyeDamage(), 1.0F);


        //Blocks

        addDefaultBlock(Blocks.JUKEBOX, 1.0F);
        addDefaultBlock(Blocks.WEB, 1.0F);
        addDefaultBlock(Blocks.TALLGRASS, 1.0F);
        addDefaultBlock(Blocks.DEADBUSH, 1.0F);
        addDefaultBlock(Blocks.WOOL, 1.0F);
        addDefaultBlock(Blocks.YELLOW_FLOWER, 1.0F);
        addDefaultBlock(Blocks.RED_FLOWER, 1.0F);
        addDefaultBlock(Blocks.BROWN_MUSHROOM, 1.0F);
        addDefaultBlock(Blocks.RED_MUSHROOM, 1.0F);
        addDefaultBlock(Blocks.TNT, 1.0F);
        addDefaultBlock(Blocks.BOOKSHELF, 1.0F);
        addDefaultBlock(Blocks.LADDER, 1.0F);
        addDefaultBlock(Blocks.WOODEN_PRESSURE_PLATE, 1.0F);
        addDefaultBlock(Blocks.REDSTONE_TORCH, 1.0F);
        addDefaultBlock(Blocks.SNOW_LAYER, 1.0F);
        addDefaultBlock(Blocks.ICE, 1.0F);
        addDefaultBlock(Blocks.SNOW, 1.0F);
        addDefaultBlock(Blocks.ACACIA_FENCE, 1.0F);
        addDefaultBlock(Blocks.BIRCH_FENCE, 1.0F);
        addDefaultBlock(Blocks.DARK_OAK_FENCE, 1.0F);
        addDefaultBlock(Blocks.JUNGLE_FENCE, 1.0F);
        addDefaultBlock(Blocks.OAK_FENCE, 1.0F);
        addDefaultBlock(Blocks.SPRUCE_FENCE, 1.0F);
        addDefaultBlock(Blocks.PUMPKIN, 1.0F);
        addDefaultBlock(Blocks.LIT_PUMPKIN, 1.0F);
        addDefaultBlock(Blocks.TRAPDOOR, 1.0F);
        addDefaultBlock(Blocks.BROWN_MUSHROOM_BLOCK, 1.0F);
        addDefaultBlock(Blocks.RED_MUSHROOM_BLOCK, 1.0F);
        addDefaultBlock(Blocks.MELON_BLOCK, 1.0F);
        addDefaultBlock(Blocks.ACACIA_FENCE_GATE, 1.0F);
        addDefaultBlock(Blocks.BIRCH_FENCE_GATE, 1.0F);
        addDefaultBlock(Blocks.DARK_OAK_FENCE_GATE, 1.0F);
        addDefaultBlock(Blocks.JUNGLE_FENCE_GATE, 1.0F);
        addDefaultBlock(Blocks.OAK_FENCE_GATE, 1.0F);
        addDefaultBlock(Blocks.SPRUCE_FENCE_GATE, 1.0F);
        addDefaultBlock(Blocks.WATERLILY, 1.0F);
        addDefaultBlock(Blocks.WOODEN_SLAB, 1.0F);
        addDefaultBlock(Blocks.WOODEN_BUTTON, 1.0F);

        String[] defaults = new String[_defaults.size()];
        _defaults.toArray(defaults);

        HashMap<Ingredient, Float> items = loadItemStackIntMap("Custom Buoyancy", Configuration.CATEGORY_GENERAL, "Config for adding custom buoyancy values examples:  minecraft:stone:1=1.0 or ore:cobbestone=1.0", defaults);

        for (Map.Entry<Ingredient, Float> entry : items.entrySet()) {
            for (ItemStack stack : entry.getKey().getMatchingStacks()) {
                //Don't worry about adding map entries that are already the same as the default.
                if (buoyancy.getDefaultValue().equals(entry.getValue()))
                    continue;
                buoyancy.put(stack, entry.getValue());
            }
        }
        config.save();

    }
}
