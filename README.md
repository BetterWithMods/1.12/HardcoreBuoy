# Hardcore Buoy

Makes it so items float or not depending on their material properties.

Separated this feature from Better With Mods as it is easier and less hacky to implement it as a core mod.


Intermod Communications Message

```
modid: "hardcorebuoy"
key: "buoy"
nbt: {

stack : <itemstack serialization tag> 
//or
ore : <oredict entry>

value: <float between -1.0 and 1.0> 
//-1.0 is the default for nonregistered items and will always sink,
//1.0 will float on top of the water.
//Anything in between is pretty undetermined, 0 might not move at all?
```  
